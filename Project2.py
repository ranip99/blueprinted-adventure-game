#  File: Project2.py
#  Description: Create a blueprinted game like zork
#  Student's Name: Rani Patel
#  Student's UT EID: rjp2524
#  Course Name: CS 303E 
#  Unique Number: 50191
#
#  Date Created:5/4/2020
#  Date Last Modified: 5/6/2020


#this class allows each room to be made an instance
class Room:


    #initializer
    def __init__(self,name,north,east,south,west,up,down,contents):
        
        self.north, self.east, self.south, self.west = north, east, south, west
        self.name, self.up, self.down, self.contents =  name, up, down, contents


    #this method can be used to display any of the rooms
    def displayRoom(self):

        #a way to know what each part of the list is meant for
        compass = ["name","north","east","south","west","up","down"]

        #the information of a specific room in a list
        directionSelf = [self.name,self.north,self.east,self.south,
                         self.west,self.up,self.down]
        
        print("Room name: ", self.name)

        #this loop goes on for each of the directions
        for i in range(1,len(directionSelf)):
            
            #makes sure that their is a room in that direction
            if directionSelf[i] != None:
                
                #for "up" need to print above
                if compass[i] == "up":
                    #prints the room that is above
                    print(format("   Room above: ","22s"), directionSelf[i])
                
                elif compass[i] == "down":
                    print(format("   Room below: ","22s"), directionSelf[i])

                #for all other directions prints the direction directly from compass
                else:
                    print(format("   Room to the " + str(compass[i]) + ": ","22s"),
                          directionSelf[i])

        #prints the contents of the room
        if self.contents != None:
            print(format("   Room contents:","22s"), self.contents)
            
        else:
            print(format("   Room contents:","22s"), "[]")
                    
        #blank line at the end of a room description
        print()
            

#function that turns a room into a Room object
def createRoom(roomData):

    contents = []

    #includes the contents of the room in the Class Room
    for i in range (7,len(roomData)):
        if roomData[i] != None:
            
            #adds the contents into a list if there are any
            contents.append(roomData[i])
            
        else:
            contents = [None]
   
    #each part of the list is put together and referenced to the class Room
    roomParts = Room(roomData[0], roomData[1], roomData[2],
                     roomData[3], roomData[4], roomData[5], roomData[6], contents)

    #returns the object Room of a room so that the floorplan can be made in loadMap()
    return roomParts


#this allows a user to know where they are at any time
def look():

    #prints place using the method from the class Room that returns just the name
    print("You are currently in the",current.name + ".")

    print("Contents of the room: ")
    #prints the contents of the room from a loop through the contents from Room
    for i in range(len(current.contents)):
        print("   " + current.contents[i])

    #no contents
    if current.contents == []:
        print("   None") 


#allows for the string of a room name to return the class Room object
def getRoom(name):

    #loop that continues for the number of rooms
    for i in range(len(floorPlan)):
        
        #goes through the list to match the string name
        if name == floorPlan[i].name:

            #gets the whole Room object from the floor plan
            room = floorPlan[i]
            
    #returns the room as an object to be used in move()
    return room
    

#sets the new current place to where the user moves to
def move(direction):

    #using the method from Room the direction moved is matched to what the new place is
    if direction == "north":
        newPlace = current.north
    elif direction == "east":
        newPlace = current.east
    elif direction == "south":
        newPlace = current.south
    elif direction == "west":
        newPlace = current.west
    elif direction == "up":
        newPlace = current.up
    elif direction == "down":
        newPlace = current.down

    #initializer
    newPlaceObject = ""

    #the new room is found as a Room object and replaces current
    if newPlace != None:
        newPlaceObject = getRoom(newPlace)
        print("You are now in the", newPlace + ".")

    #if there is nothing in that direction let's the user know and does not change current
    else:
        newPlaceObject = current
        print("You can't move in that direction.")
            
    return newPlaceObject 


#shows the possibilities for each direction available for each room
def displayAllRooms():

    #loop continues for each room in the floorplan
    for i in range(len(floorPlan)):

        #uses the method from Room to display one room at a time
        floorPlan[i].displayRoom()


#pickup any item in the current room
def pickup(item):

    #loop checks each of the contents of the room to match to your item
    for i in range(len(current.contents)):
        if current.contents[i] == item:
            
            #add to your inventory if found
            inventory.append(item)

            #remove from the room contents
            current.contents.remove(item)
            
            print("You now have the", item + ".")
            
            #quits once found
            return
        
    #if it still wasn't found in the room
    print("That item is not in this room.")

    
#drop any item in your inventory into the current room
def drop(item):

    #checks inventory for items to drop
    for i in range(len(inventory)):

        #drops from inventory and adds to current room contents
        if item == inventory[i]:
            inventory.remove(item)
            current.contents.append(item)
            
            print("You have dropped the", item + ".")
            return

    #item not in inventory
    print("You don't have that item.")

#shows what you have in your inventory
def listInventory():

    print("You are currently carrying: ")

    #empty inventory
    if inventory == []:
        print("   nothing.")

    #prints each item in the inventory
    else:
        for i in range(len(inventory)):
            print("   " + inventory[i])


#makes the floorplan from a file      
def loadMap():

    global floorPlan
    floorPlan = []

    #open and read the file with data 
    data = open("ProjectData.txt", "r")
    line = data.readline().split(",")

    #loop runs until the data is all read
    while line != ['']:

        #this loop nealty places each line in a list
        for i in range(len(line)):

            #only if the data is not None it is changed
            if line[i] != None:

                #eval gets rid of extra quotes and spaces
                line[i] = eval(line[i])

        #adds the room line to the floorplan
        floorPlan.append(createRoom(line))

        #read next line
        line = data.readline().split(",")

    #close file
    data.close()

#takes commands and runs them CLI
def main():

    global current

    global inventory
    inventory = []

    #loads the floorplan from the file
    loadMap()

    #sets the starting position
    current = floorPlan[0]

    #displays the starting position and contents
    look()

    #takes the first command
    print()
    command = input("Enter a command: ")

    #continues until user wants to exit
    while command != "exit":

        #removes extra spaces and lowercases the command
        command = command.strip().lower()

        #help command prints all the commands possible
        if command == "help":
            print()
            print("look:        display the name of the current room and its contents")
            print("north:       move north")
            print("east:        move east")
            print("south:       move south")
            print("west:        move west")
            print("up:          move up")
            print("down:        move down")
            print("inventory:   list what items you're currently carrying")
            print("get <item>:  pick up an item currently in the room")
            print("drop <item>: drop an item you're currently carrying")
            print("help:        print this list")
            print("exit:        quit the game")

        #look shows them where they are and contents
        elif command == "look":
            look()

        #if they type a direction they move
        elif command == "north" or command == "east" or command == "south" \
        or command == "west" or command == "up" or command == "down":
            current = move(command)

        #inventory command shows them what they are carrying
        elif command == "inventory":
            listInventory()

        #get calls on the pickup function
        elif "get" in command:
            #it is split so that the object cna be identified
            pickup(command.split()[1])

        #drop calls the drop function allowing them to drop item in inventory
        elif "drop" in command:
            drop(command.split()[1])

        #cheat just displays all the rooms, directions, and contents
        elif command == "cheat":
            displayAllRooms()

        #none of their commands for an option
        else:
            print("Please enter a different command.")

        #restart the loop
        print()
        command = input("Enter a command: ")

    #the user has exitted the game
    print("Quitting game.")


main()
