#  File: Project1.py
#  Description: Create a blueprinted game
#  Student's Name: Rani Patel
#  Student's UT EID: rjp2524
#  Course Name: CS 303E 
#  Unique Number: 50191
#
#  Date Created:4/14/2020
#  Date Last Modified: 4/20/2020


#this class allows each room to be made an instance
class Room:


    #initializer
    def __init__(self,name,north,east,south,west,up,down):
        
        self.north, self.east, self.south, self.west = north, east, south, west
        self.name, self.up, self.down =  name, up, down


    #this method can be used to display any of the rooms
    def displayRoom(self):

        #a way to know what each part of the list is meant for
        compass = ["name","north","east","south","west","up","down"]

        #the information of a specific room in a list
        directionSelf = [self.name,self.north,self.east,self.south,
                         self.west,self.up,self.down]
        
        print("Room name: ", self.name)

        #this loop goes on for each of the directions
        for i in range(1,len(directionSelf)):
            
            #makes sure that their is a room in that direction
            if directionSelf[i] != None:
                
                #for "up" need to print above
                if compass[i] == "up":
                    #prints the room that is above
                    print(format("   Room above: ","22s"), directionSelf[i])
                
                elif compass[i] == "down":
                    print(format("   Room below: ","22s"), directionSelf[i])

                #for all other directions prints the direction directly from compass
                else:
                    print(format("   Room to the " + str(compass[i]) + ": ","22s"),
                          directionSelf[i])
                    
        #blank line at the end of a room description
        print()
            

#function that turns a room into a Room object
def createRoom(roomData):

    #each part of the list is put together and referenced to the class Room
    roomParts = Room(roomData[0], roomData[1], roomData[2],
                     roomData[3], roomData[4], roomData[5], roomData[6])

    #returns the object Room of a room so that the floorplan can be made in loadMap()
    return roomParts


#this allows a user to know where they are at any time
def look():

    #prints place using the method from the class Room that returns just the name
    print("You are currently in the",current.name + ".")

    
#allows for the string of a room name to return the class Room object
def getRoom(name):

    #all the string names of the rooms are listed   
    rooms = ["Living Room", "Dining Room", "Kitchen", "Upper Hall",
             "Bathroom", "Small Bedroom", "Master Bedroom"]

    #loop that continues for the number of rooms
    for i in range(len(rooms)):
        
        #goes through the list to match the string name
        if name == rooms[i]:

            #gets the whole Room object from the floor plan
            room = floorPlan[i]
    #returns the room as an object to be used in move()
    return room
    

#sets the new current place to where the user moves to
def move(direction):

    #using the method from Room the direction moved is matched to what the new place is
    if direction == "north":
        newPlace = current.north
    elif direction == "east":
        newPlace = current.east
    elif direction == "south":
        newPlace = current.south
    elif direction == "west":
        newPlace = current.west
    elif direction == "up":
        newPlace = current.up
    elif direction == "down":
        newPlace = current.down

    #initializer
    newPlaceObject = ""

    #the new room is found as a Room object and replaces current
    if newPlace != None:
        newPlaceObject = getRoom(newPlace)
        print("You are now in the", newPlace + ".")

    #if there is nothing in that direction let's the user know and does not change current
    else:
        newPlaceObject = current
        print("You can't move in that direction.")
            
    return newPlaceObject 


#shows the possibilities for each direction available for each room
def displayAllRooms():

    #loop continues for each room in the floorplan
    for i in range(len(floorPlan)):

        #uses the method from Room to display one room at a time
        floorPlan[i].displayRoom()
                   
                   

##########################################################################
###     All code below this is given to you.  DO NOT EDIT IT unless    ###
###     you need to adjust the indentation to match the indentation    ###
###     of the rest of your code.                                      ###
##########################################################################
        
def loadMap():

    global floorPlan    # make the variable "floorPlan" a global variable
    
    room1 = ["Living Room","Dining Room",None,None,None,"Upper Hall",None]
    room2 = ["Dining Room",None,None,"Living Room","Kitchen",None,None]
    room3 = ["Kitchen",None,"Dining Room",None,None,None,None]
    room4 = ["Upper Hall","Bathroom","Small Bedroom","Master Bedroom",None,None,"Living Room"]
    room5 = ["Bathroom",None,None,"Upper Hall",None,None,None]
    room6 = ["Small Bedroom",None,None,None,"Upper Hall",None,None]
    room7 = ["Master Bedroom","Upper Hall",None,None,None,None,None]

    floorPlan = [createRoom(room1),createRoom(room2),createRoom(room3),createRoom(room4),
                 createRoom(room5),createRoom(room6),createRoom(room7)]

def main():

    global current      # make the variable "current" a global variable
    
    loadMap()
    
    displayAllRooms()

    # TEST CODE:  walk around the house
    
    current = floorPlan[0]      # start in the living room
    look()                      # Living Room
    
    current = move("south")     # can't move this direction
    current = move("west")      # can't move this direction
    current = move("north")     # Dining Room
    current = move("south")     # Living Room
    current = move("up")        # Upper Hall
    look()                      # Upper Hall
    current = move("east")      # Small Bedroom
    current = move("east")      # can't move this direction
    current = move("west")      # Upper Hall
    current = move("south")     # Master Bedroom
    current = move("north")     # Upper Hall
    current = move("north")     # Bathroom
    current = move("south")     # Upper Hall
    look()                      # Upper Hall
    current = move("west")      # can't move this direction
    look()                      # still in the Upper Hall
    current = move("down")      # Living Room
    current = move("north")     # Dining Room
    current = move("west")      # Kitchen
    current = move("north")     # can't move this direction

main()
